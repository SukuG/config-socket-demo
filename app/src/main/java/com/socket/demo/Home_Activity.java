package com.socket.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.nkzawa.emitter.Emitter;

import org.json.JSONException;
import org.json.JSONObject;

public class Home_Activity extends AppCompatActivity {

    EditText mEditTextMessage;
    Button mButtonSend;
    TextView mTextView;
    private String TAG = "Home_Activity";
    //chatting url demo// https://socket-io-chat.now.sh/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_);
        mTextView = (TextView) findViewById(R.id.editText2);

        mButtonSend = (Button) findViewById(R.id.button);
        mEditTextMessage = (EditText) findViewById(R.id.editTextMessage);
        mButtonSend.setOnClickListener(onClickListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //connect socket and listen with key once called onResume.
        //socket status
        if (!SocketApplication.getInstance().isConnected()) {
            boolean status = SocketApplication.getInstance().connectSocketIfNot();
            Log.d(TAG, getString(R.string.socket_connect_status) + status);
        } else
            Log.d(TAG, getString(R.string.socket_already_connected));
        //listening message with key name=new message;
        SocketApplication.getInstance().mSocket.on("new message", onNewMessage);

    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            attemptSend();
        }
    };

    //this is model for emitting messge. below method won't wokr. If u want work state then follow https://github.com/nkzawa/socket.io-android-chat
    private void attemptSend() {
        String message = mEditTextMessage.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            return;
        }
        mEditTextMessage.setText("");
        SocketApplication.getInstance().mSocket.emit("new message", message);//message- u can send String ONLY
    }

    //once get new msg then update UI.
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //retrive data from stirng argument.
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String message;
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                    } catch (JSONException e) {
                        return;
                    }
                    setMessage(username, message);
                }
            });
        }
    };

    private void setMessage(String userName, String message) {
        String outPutString = userName + " : " + message;
        mTextView.setText(outPutString);
        Log.d(TAG, "setMessage:" + outPutString);
    }


    @Override
    protected void onPause() {
        super.onPause();
        //disconnect socket once called onPause.
        if (SocketApplication.getInstance().isConnected()) {
            SocketApplication.getInstance().mSocket.disconnect();
            Log.d(TAG, getString(R.string.socket_disconnected));
        } else
            Log.d(TAG, getString(R.string.socket_not_disconnected));

        //you should off listening socket once called onPause.
        SocketApplication.getInstance().mSocket.off("new message", onNewMessage);
    }
}
