package com.socket.demo;

import android.app.Application;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

/**
 * Created by sukumar on 20-11-2017
 */

public class SocketApplication extends Application {

    private String TAG = "SocketApplication";
    public Socket mSocket;
    private static SocketApplication mSocketApplication;

    public static SocketApplication getInstance() {
        if (mSocketApplication == null)
            mSocketApplication = new SocketApplication();
        return mSocketApplication;
    }

    Emitter.Listener onConnectListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onConnectListener called");
        }
    };

    Emitter.Listener onErrorListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onErrorListener called");
        }
    };
    Emitter.Listener onConnectionTimeOutListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onConnectionTimeOutListener called");

        }
    };
    Emitter.Listener onDisConnectionListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onDisConnectionListener called");
        }
    };

    Emitter.Listener onConnectErrorListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.d(TAG, "onConnectErrorListener called");
        }
    };

    {
        try {
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            opts.reconnection = true;
            mSocket = IO.socket("https://socket-io-chat.now.sh/");//socket url


        } catch (URISyntaxException e) {
            Log.d(TAG, "URISyntaxException" + e.getMessage());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSocketApplication = this;
        //only one listener called. i.e. If socket connected then called onConnectListener else respective error listener called.
        //so we can detect here itself whether socket connected or not.
        mSocket.on(mSocket.EVENT_CONNECT, onConnectListener);
        mSocket.on(mSocket.EVENT_CONNECT_ERROR, onConnectErrorListener);
        mSocket.on(mSocket.EVENT_CONNECT_TIMEOUT, onConnectionTimeOutListener);
        mSocket.on(mSocket.EVENT_DISCONNECT, onDisConnectionListener);
        mSocket.on(mSocket.EVENT_ERROR, onErrorListener);
        //connect soket now
        mSocket.connect();

    }


    public boolean isConnected() {
        if (mSocket != null) {
            if (mSocket.connected())
                return true;
            else
                return false;
        } else {
            Log.d(TAG, "mSocket is null");
            return false;
        }
    }

    public boolean connectSocketIfNot() {
        Log.d(TAG, "Socket trying to connecting");
        if (mSocket != null) {
            if (!mSocket.connected()) {
                mSocket.connect();
                Log.d(TAG, "connectSocketIf  connected");
                return true;
            } else
                return false;
        } else
            return false;
    }
}
